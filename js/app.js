var app = {
  initDone: false,
  lines: [],
  pencil: null,
  pos: null,
  movePos: {},

  modes: ['line','erase','select','pencil','move'],
  activeMode: 'line',
  selectedLineIndex: null,
  init: function() {
    var self = this;
    if(self.initDone)
      return;
    self.bindToolbarEvents();
    self.bindDrawAreaEvents();
    self.initDone = true;
  },

  toolbarClickActionOverride: {
    'erase': function(self) {
      let clickContinue = true;
      if (self.selectedLineIndex !== null) {
        // delete selected line
        self.eraseLine(self.selectedLineIndex);
        self.selectedLineIndex = null;
        self.render();
        clickContinue = false;
      }
      return clickContinue;
    }
  },

  bindToolbarEvents: function() {
    var self = this;
    for (var i = 0; i < self.modes.length; i++) {
      var mode = self.modes[i];
      // closure to preserve variable scope during loop
      (function(mode){
        document.getElementById('btn-'+mode).addEventListener('click', function() {
          var clickContinue = true;
          if (self.toolbarClickActionOverride[mode]) {
            clickContinue = self.toolbarClickActionOverride[mode](self);
          }
          if (clickContinue) {
            self.activeMode = mode;
            self.pos = null;
            self.updateToolbarState();
          }
        });
      })(mode);
    }
  },
  updateToolbarState: function() {
    var self = this;
    // reset toolbar, highlight active mode
    for (var i = 0; i < self.modes.length; i++) {
      var mode = self.modes[i];
      document.getElementById('btn-'+mode).className = self.activeMode === mode ? 'active' : '';
    }
  },

  drawAreaEventClickActions: {
    'line': function(self,x,y){
      if(!self.pos) {
        // save first click of the line
        self.pos = [ x, y ];
      } else {
        // create the line and add to the list
        var x0 = self.pos[0], y0 = self.pos[1];
        var length = Math.sqrt((x - x0) * (x - x0) + (y - y0) * (y - y0));
        var line = new Line(x0, y0, x, y, length);
        self.lines.push(line);
        self.pos = null;
      }
    },
    'erase': function(self,x,y) {
      if (self.lines.length > 0) {
        var minSquareDistance, closestIndex;
        self.lines.forEach(function(line, index) {
          var squareDistance = line.squareDistanceFrom(x, y);
          if(index === 0 || squareDistance < minSquareDistance) {
            minSquareDistance = squareDistance;
            closestIndex = index;
          }
        });
        self.eraseLine(closestIndex);
      }
    },
    'select': function(self,x,y) {
      self.selectedLineIndex = self.getClosestLineIndex(self,x,y);
    }

  },

  getClosestLineIndex: function(self,x,y) {
    if (self.lines.length > 0) {
      var minDistance, closestIndex;
      self.lines.forEach(function(line, index) {
        var distance = line.distanceFrom(x, y);
        if(index === 0 || distance < minDistance) {
          minDistance = distance;
          closestIndex = index;
        }
      });
      return minDistance < 11 ? closestIndex : null;
    }
  },

  bindDrawAreaEvents: function() {
    var self = this;
    var canvas = document.getElementById('canvas');
    canvas.addEventListener('click', function(e) {
      var mode = self.activeMode;
      var x = e.offsetX, y = e.offsetY;
      if (self.drawAreaEventClickActions && self.drawAreaEventClickActions[mode]) {
        self.drawAreaEventClickActions[mode](self,x,y);
      }
      self.render();
    });

    // mouse move events for pencil and move
    var mouseMove = function(e) {
      var x = e.offsetX, y = e.offsetY;
      if (self.activeMode === 'pencil') {
        self.pencilLine.drawSegment(self.ctx(),x,y);
      } else if (self.activeMode === 'move') {
        var dx = x - self.movePos.x;
        var dy = y - self.movePos.y;
        var line = self.lines[self.selectedLineIndex];
        line.move(dx,dy);
        self.render();
        self.movePos.x = x;
        self.movePos.y = y;
      }
    };

    // mouse down events for pencil and move
    var mouseDown = function(e) {
      var x = e.offsetX, y = e.offsetY;
      if (self.activeMode === 'pencil') {
        self.pencilLine = new PencilLine(x,y);
        canvas.addEventListener('mousemove',mouseMove);
        canvas.addEventListener('mouseout', mouseUp);
      } else if (self.activeMode === 'move') {
        // select the line
        self.selectedLineIndex = self.getClosestLineIndex(self,x,y);
        if (self.selectedLineIndex !== null) {
          // draw ends on the line
          self.lines[self.selectedLineIndex].drawEnds(self.ctx());
          // starting move position
          self.movePos = {x:x,y:y};
          // listen for mouse movement while the mouse is down
          canvas.addEventListener('mousemove',mouseMove);
        }
      }
    };

    // mouse up events for pencil and move
    var mouseUp = function() {
      if (self.activeMode === 'pencil') {
        // remove the move and out listeners, store the line
        canvas.removeEventListener('mousemove',mouseMove);
        canvas.removeEventListener('mouseout',mouseUp);
        self.lines.push(self.pencilLine);
      } else if (self.activeMode === 'move') {
        // remove the move listener, deselect the line
        canvas.removeEventListener('mousemove',mouseMove);
        self.selectedLineIndex = null;
      }
    };

    // add mouse down and up listeners
    canvas.addEventListener('mousedown', mouseDown);
    canvas.addEventListener('mouseup', mouseUp);
  },

  eraseLine: function(index) {
    var self = this;
    self.lines.splice(index, 1);
  },

  ctx: function() {
    var canvas = document.getElementById('canvas');
    return canvas.getContext('2d');
  },

  render: function() {
    var self = this;
    var ctx = self.ctx();
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    self.lines.forEach(function(line) {
      line.draw(ctx);
    });
    if (self.selectedLineIndex !== null) {
      self.lines[self.selectedLineIndex].drawEnds(ctx);
    }
  },
};
