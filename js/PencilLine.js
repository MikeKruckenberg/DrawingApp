var PencilLine = function(x, y) {
  this.x = x;
  this.y = y;
  this.coordinates = [{x:x,y:y}];
  this.drawingFromMouse = true;
};

PencilLine.prototype = Object.create(new Line());

PencilLine.prototype.position = function(x,y) {
  this.x = x;
  this.y = y;
};

PencilLine.prototype.drawSegment = function(ctx,x,y) {
  if (this.drawingFromMouse) {
    this.coordinates.push({x:x,y:y});
  }
  ctx.beginPath();
  ctx.strokeStyle = '#000';
  ctx.lineWidth = 1;
  ctx.moveTo(this.x, this.y);
  this.position(x,y);
  ctx.lineTo(this.x, this.y);
  ctx.stroke();
};

PencilLine.prototype.draw = function(ctx) {
  var self = this;
  self.drawingFromMouse = false;
  this.position(this.coordinates[0].x,this.coordinates[0].y);
  this.coordinates.forEach(function(item) {
    self.drawSegment(ctx,item.x,item.y);
  });
};

PencilLine.prototype.move = function(dx, dy) {
  var moveCoordinates = [];
  var allInBounds = true;
  // loop through all points
  for (let item of this.coordinates) {
    let newX = item.x + dx,
        newY = item.y + dy;
    if (this.inBounds(newX, newY)) {
      moveCoordinates.push({x:newX,y:newY});
    } else {
      // stop move as soon as one point is out of bounds
      allInBounds = false;
      break;
    }
  }
  // if all points are in bounds, move the line to new coordinates
  if (allInBounds) {
    this.coordinates = moveCoordinates;
  }
};

PencilLine.prototype.drawEnds = function(ctx) {
  ctx.strokeStyle = '#000';
  ctx.lineWidth = 1;
  var drawEnd = function(x, y) {
    ctx.beginPath();
    ctx.ellipse(x, y, 5, 5, 0, 0, 2 * Math.PI);
    ctx.stroke();
  };
  drawEnd(this.coordinates[0].x,this.coordinates[0].y);
  drawEnd(this.coordinates[this.coordinates.length-1].x,this.coordinates[this.coordinates.length-1].y);
};

PencilLine.prototype.squareDistanceFrom = function(x, y) {
  var minDistance = 0,
      startX = null,
      startY = null;
  this.coordinates.forEach(function(item) {
    if (!startX) {
      startX = item.x;
      startY = item.y;
      return;
    }
    var distance = Geometry.squareDistanceToSegment(x, y, startX, startY, item.x, item.y);
    if (minDistance === 0 || distance < minDistance) {
      minDistance = distance;
    }
    startX = item.x;
    startY = item.y;
  });
  return minDistance;
};


PencilLine.prototype.distanceFrom = function(x, y) {
  var minDistance = 0,
      startX = null,
      startY = null;
  this.coordinates.forEach(function(item) {
    if (!startX) {
      startX = item.x;
      startY = item.y;
      return;
    }
    var distance = Geometry.distanceToSegment(x, y, startX, startY, item.x, item.y);
    if (minDistance === 0 || distance < minDistance) {
      minDistance = distance;
    }
    startX = item.x;
    startY = item.y;
  });
  return minDistance;
};
